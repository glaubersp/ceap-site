@extends('_layouts.master')
@section('body')
<div class="container" style="margin-top:50px">
    <div class="jumbotron" style="text-align:center">
        <div style="display: flex;align-items: center;justify-content: center;">
            <h1>Sistema de Acompanhamento de Ações Escolares</h1>
        </div>
        <p>Apoiando escolas no planejamento e monitoramento coletivo de ações e tomada de decisões</p>
    </div>
    <div class="btn-group btn-group-justified" style="text-align:center" role="group" aria-label="">
        <div class="col-md-4 col-sm-4" style="margin-bottom: 10px;"><a class="btn btn-primary btn-lg" href="https://www.sae.voluta.com.br" role="button">Acessar</a></div>
        <div class="col-md-4 col-sm-4" style="margin-bottom: 10px;"><a class="btn btn-primary btn-lg" href="{{ $page->baseUrl.'/sae' }}" role="button">Sobre</a></div>
        <div class="col-md-4 col-sm-4" style="margin-bottom: 10px;"><a class="btn btn-primary btn-lg" href="{{ $page->baseUrl.'/tutoriais' }}" role="button">Tutoriais</a></div>
    </div>
    <div class="panel-body">
        <div class="col-sm-12 col-md-12">
            <div class="thumbnail">
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe src="https://player.vimeo.com/video/259217203?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                    <p><a href="https://vimeo.com/259217203">Introdu&ccedil;&atilde;o ao Sistema SAE</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection