<?php

return [
    'subject' => [
        'prefix' => '[CEAP Site]'
    ],
    'emails' => [
        'to'   => 'tel@amiel.info',
        'from' => 'contato@voluta.com.br'
    ],
    'messages' => [
        'error'   => 'Por favor, verifique se você preencheu todos os campos.',
        'success' => 'Sua mensagem foi enviada com sucesso.',
        'captcha-error' => 'O valor da conta está errado!',
    ],
    'fields' => [
        'name'     => 'Nome',
        'email'    => 'Email',
        'phone'    => 'Telefone',
        'subject'  => 'Assunto',
        'message'  => 'Mensagem',
        'btn-send' => 'Enviar',
        'btn-clean' => 'Limpar',
        'panel-title' => 'Contato',
        'captcha-title' => 'Resolva',
    ]
];
