@extends('_layouts.master')

@section('body')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h1 class="panel-title">Tutoriais</h1>
                </div>
                <div class="panel-body">

                    <h2>Tutoriais</h2>
                    <p>Os vídeos para a navegação e administração do sistema estão disponíveis abaixo. Para ativar a legenda do vídeo, clique em 'CC'.</p>
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="introducao">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#introducaoPanel" aria-expanded="true" aria-controls="introducaoPanel">
                                        Introdução (2 vídeos)
                                    </a>
                                </h4>
                            </div>
                            <div id="introducaoPanel" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="introducao">
                                <div class="panel-body">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe src="https://player.vimeo.com/video/259217203?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <p><a href="https://vimeo.com/259217203">Introdu&ccedil;&atilde;o ao Sistema SAE</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe src="https://player.vimeo.com/video/260047638?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <p><a href="https://vimeo.com/260047638">Navega&ccedil;&atilde;o Geral do Sistema SAE</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="acoes">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#acoesPanel" aria-expanded="false" aria-controls="acoesPanel">
                                        Ações (2 vídeos)
                                    </a>
                                </h4>
                            </div>
                            <div id="acoesPanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="acoes">
                                <div class="panel-body">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe src="https://player.vimeo.com/video/260486443?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <p><a href="https://vimeo.com/260486443">A&ccedil;&otilde;es no Sistema SAE (Parte 1/2)</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe src="https://player.vimeo.com/video/260486514?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <p><a href="https://vimeo.com/260486514">A&ccedil;&otilde;es no Sistema SAE (Parte 2/2)</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="objetivos">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#objetivosPanel" aria-expanded="false" aria-controls="objetivosPanel">
                                        Objetivos (2 vídeos)
                                    </a>
                                </h4>
                            </div>
                            <div id="objetivosPanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="objetivos">
                                <div class="panel-body">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe src="https://player.vimeo.com/video/260571761?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <p><a href="https://vimeo.com/260571761">Objetivos no Sistema SAE (Parte 1/2)</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe src="https://player.vimeo.com/video/260571835?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <p><a href="https://vimeo.com/260571835">Objetivos no Sistema SAE (Parte 2/2)</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="indicadores">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#indicadoresPanel" aria-expanded="false" aria-controls="indicadoresPanel">
                                        Indicadores (3 vídeos)
                                    </a>
                                </h4>
                            </div>
                            <div id="indicadoresPanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="indicadores">
                                <div class="panel-body">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe src="https://player.vimeo.com/video/260569161?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <p><a href="https://vimeo.com/260569161">Indicadores no Sistema SAE (Parte 1/3)</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe src="https://player.vimeo.com/video/260569205?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <p><a href="https://vimeo.com/260569205">Indicadores no Sistema SAE (Parte 2/3)</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe src="https://player.vimeo.com/video/260569268?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <p><a href="https://vimeo.com/260569268">Indicadores no Sistema SAE (Parte 3/3)</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="combinados">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#combinadosPanel" aria-expanded="false" aria-controls="combinadosPanel">
                                        Combinados (1 vídeo)
                                    </a>
                                </h4>
                            </div>
                            <div id="combinadosPanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="combinados">
                                <div class="panel-body">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe src="https://player.vimeo.com/video/260571217?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <p><a href="https://vimeo.com/260571217">Combinados no Sistema SAE</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="administracao">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#administracaoPanel" aria-expanded="false" aria-controls="administracaoPanel">
                                        Administração (2 vídeos)
                                    </a>
                                </h4>
                            </div>
                            <div id="administracaoPanel" class="panel-collapse collapse" role="tabpanel" aria-labelledby="administracao">
                                <div class="panel-body">
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe src="https://player.vimeo.com/video/260489349?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <p><a href="https://vimeo.com/260489349">Administra&ccedil;&atilde;o do Sistema SAE (Parte 1/2)</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6">
                                        <div class="thumbnail">
                                            <div class="embed-responsive embed-responsive-16by9">
                                                <iframe src="https://player.vimeo.com/video/260489586?byline=0" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                                <p><a href="https://vimeo.com/260489586">Administra&ccedil;&atilde;o do Sistema SAE (Parte 2/2)</a> from <a href="https://vimeo.com/iniciativaea">Tel Amiel</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

