<?php

return [
    'build' => [
        'destination' => 'build_{env}/site/',
    ],
    'baseUrl' => 'https://www.voluta.com.br/site',
    'production' => true,
    'collections' => [],
    'index_title' => 'Bem-vindo ao VOLUTA',
];
